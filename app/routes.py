from app import app
from flask import render_template, url_for, flash, redirect
from app.forms import LoginForm, CreateForm, SubmitPost, UpdateProfile, CommentOnPost, SearchText
from flask_login import current_user, login_user, logout_user
from flask_login import login_required
from app.models import User, Post, Comment
from flask import request
from werkzeug.urls import url_parse
from app import db
from app.forms import ResetPasswordRequestForm
from app.email import send_password_reset_email
from app.forms import ResetPasswordForm
from sqlalchemy import func
from elasticsearch import Elasticsearch

@app.route('/', methods = ['GET', 'POST'])
@app.route('/index', methods = ['GET', 'POST'])
def index():
    search = SearchText()

    comment = Comment.query.with_entities( func.count( Comment.body ) ).group_by( Comment.post_id ).all()
    page = request.args.get( 'page', 1, type = int )
    posts = Post.query.order_by(Post.timestamp.desc()).paginate(
        page, 10, False)
    post_for_search = Post.query.all()
    next_url = url_for('index', page = posts.next_num) \
        if posts.has_next else None
    if request.method == 'POST' and search.validate():
        es = Elasticsearch("http://localhost:9200")
        search_result = list()
        es.indices.create( index = 'search_post', ignore = 400 )
        for post_data in post_for_search:
            payload = { "post_title" : str( post_data.title ), "post_url" : str( post_data.url ) }
            es.index( index = 'search_post', doc_type = 'post', id = post_data.id, body = payload )
        search_result = es.search( index = 'search_post', body = { "from" : 0 , "size" : 0 , "query":{ "match":{ "post_title":search.search.data } } } )
        all_search_result = es.search( index = 'search_post', body = {"from": 0,"size": search_result['hits']['total'], "query":{ "match":{ "post_title":search.search.data}}})
        return render_template( 'search_result.html', all_search_result = all_search_result)
    return render_template( 'index.html', posts = posts.items, next_url = next_url, comment = comment, search = search)


@app.route('/login', methods = ['GET', 'POST'])
def login():
    form = LoginForm()
    create_form = CreateForm()
    if current_user.is_authenticated:
        return redirect( url_for( 'index' ) )
    if request.method == 'POST' and form.validate():
        user = User.query.filter_by( username = form.username.data ).first()
        if user is None or not user.check_password( form.password.data ):
            flash('Invalid username or password')
            return redirect( url_for( 'login' ) )
        flash( 'Login requested for user {}'.format( form.username.data ) )
        login_user( user )
        next_page = request.args.get( 'next' )
        if not next_page or url_parse( next_page ).netloc != '':
            next_page = url_for( 'index' )
        return redirect( next_page )

    if request.method == 'POST' and create_form.validate():
        user = User( username = create_form.create_username.data )
        user.set_password( create_form.create_password.data )
        db.session.add( user )
        db.session.commit()
        flash( 'Congratulations, you are now a registered user!' )
        return redirect('/login' )
    return render_template( 'login.html', form = form, create_form = create_form )

@app.route('/logout')
def logout():
    logout_user()
    return redirect( url_for( 'index' ) )

@app.route('/submit', methods = ['GET', 'POST'])
@login_required
def submit():
    posts = SubmitPost()
    if posts.validate_on_submit():
        post = Post(title = posts.title.data, url = posts.url.data,original_domain = find_original_domain( posts.url.data ), text = posts.text.data, author=current_user)
        db.session.add(post)
        db.session.commit()
        return redirect('/index')
    return render_template('submit.html', posts = posts)

@app.route('/users/<username>', methods = ['GET', 'POST'])
def user_profile(username):
    user = UpdateProfile()
    user_info = User.query.filter_by(username = username).first_or_404()
    if request.method == "POST" and user.validate():
        current_user.about = user.about.data
        current_user.email = user.email.data
        current_user.showdead = user.showdead.data
        current_user.noprocrast = user.noprocrast.data
        current_user.maxvisit = user.maxvisit.data
        current_user.minaway = user.minaway.data
        current_user.delay = user.delay.data
        db.session.commit()
        return redirect( '/index' )

    return render_template('profile.html', user = user, user_info = user_info)

@app.route('/comments/<post_id>', methods = ['GET', 'POST'])
def create_comment(post_id):
    comment = CommentOnPost()
    all_comment_on_post = Comment.query.filter_by(post_id = post_id).all()
    post = Post.query.filter_by(id = post_id).first_or_404()
    if request.method == 'POST' and comment.validate() and current_user.is_authenticated:
        comment_query = Comment(body = comment.body.data, comment_post = post, comment_user = current_user)
        db.session.add(comment_query)
        db.session.commit()
        return redirect(url_for('create_comment', post_id=post_id))

    return render_template('discussion.html',comment = comment, all_comment_on_post = all_comment_on_post, post = post)

@app.route('/reset_password_request', methods=['GET', 'POST'])
def reset_password_request():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = ResetPasswordRequestForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user:
            send_password_reset_email(user)
        flash('Check your email for the instructions to reset your password or update your profile')
        return redirect(url_for('login'))
    return render_template('reset_password_request.html', form=form)

@app.route('/reset_password/<token>', methods=['GET', 'POST'])
def reset_password(token):
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    user = User.verify_reset_password_token(token)
    if not user:
        return redirect(url_for('index'))
    form = ResetPasswordForm()
    if form.validate_on_submit():
        user.set_password(form.password.data)
        db.session.commit()
        flash('Your password has been reset.')
        return redirect(url_for('login'))
    return render_template('reset_password.html', form=form)

@app.route('/newcomments', methods=['GET', 'POST'])
def show_new_comments():
    comments = Comment.query.order_by(Comment.timestamp.desc()).all()
    return render_template('new_comments.html', comments = comments)

@app.route('/vote', methods = ['GET', 'POST'])
def vote():
    print('u call me fetch')
    return redirect('index')





def find_original_domain(n):
  if '//' in n:
    val = n.find('//')
    print(val)
    s2 = n[val+2:]
    print(s2)
    val2 = s2.find('/')
    print(val2)
    s3 = s2[:val2]
    return s3
  else :
    val2 = n.find('/')
    s3 = n[0:val2]
    return s3
  return n


# DATABASE_URL=mysql+pymysql://hackernews:suraj326@localhost:3306/hackernews