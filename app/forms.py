from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms import TextAreaField, RadioField
from wtforms.validators import DataRequired, Email, EqualTo
from app.models import User

class LoginForm(FlaskForm):
    username = StringField('username : ', validators=[DataRequired()])
    password = PasswordField('password : ', validators=[DataRequired()])
    submit = SubmitField('login')

class CreateForm(FlaskForm):
    create_username = StringField('username : ', validators=[DataRequired()])
    create_password = PasswordField('password : ', validators=[DataRequired()])
    submit = SubmitField('create account')

    def validate_username(self, username):
        user = User.query.filter_by(username = username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

class SubmitPost(FlaskForm):
    title = StringField('title', validators=[DataRequired()])
    url = StringField('url', validators=[DataRequired()])
    text = TextAreaField('text' )
    submit = SubmitField('submit')

class UpdateProfile(FlaskForm):
    about = TextAreaField('about:')
    email = StringField('email:')
    showdead = RadioField('showdead:', choices=[('no','No'),('yes','Yes')])
    noprocrast =  RadioField("noprocrast:", choices=[('no','No'),('yes','Yes')])
    maxvisit = StringField('maxvisit:')
    minaway = StringField('minaway:')
    delay = StringField('delay')
    submit = SubmitField('update')

class CommentOnPost(FlaskForm):
    body = TextAreaField('comment-section', validators=[DataRequired()])
    submit = SubmitField('comment')

class ResetPasswordRequestForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Request Password Reset')

class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Request Password Reset')

class SearchText(FlaskForm):
    search = StringField('sersch')