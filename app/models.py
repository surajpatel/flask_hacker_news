from app import db
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import login
from datetime import datetime
from time import time
import jwt
from app import app

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key = True)
    username = db.Column(db.String(64), index = True, unique = True)
    password_hash = db.Column(db.String(128))
    karma = db.Column(db.Integer, default = 1)
    timestamp = db.Column(db.DateTime, index = True, default = datetime.now)
    about = db.Column(db.String(140))
    email = db.Column(db.String(30))
    showdead = db.Column(db.String(10))
    noprocrast = db.Column(db.String(10))
    maxvisit = db.Column(db.Integer, default = 20)
    minaway = db.Column(db.Integer, default = 180)
    delay = db.Column(db.Integer, default = 0)
    posts = db.relationship('Post', backref='author', lazy='dynamic')
    user_comments = db.relationship("Comment", backref="comment_user", lazy="dynamic")
    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            app.config['SECRET_KEY'], algorithm='HS256').decode('utf-8')

    @staticmethod
    def verify_reset_password_token(token):
        try:
            id = jwt.decode(token, app.config['SECRET_KEY'],
                            algorithms=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)

@login.user_loader
def load_user(id):
    return User.query.get(int(id))

class Post(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    title = db.Column(db.String(80), index = True, unique = True)
    url = db.Column(db.String(50), index = True, unique = True)
    original_domain = db.Column(db.String(50), index = True )
    text = db.Column(db.String(130), default = "")
    timestamp = db.Column(db.DateTime, index = True, default = datetime.now)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    post_comments = db.relationship("Comment", backref="comment_post", lazy="dynamic")
    def __repr__(self):
        return '<Post {}>'.format(self.title)

class Comment(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    body = db.Column(db.String(140), index = True)
    timestamp = db.Column(db.DateTime, index = True, default = datetime.now)
    post_id = db.Column(db.Integer, db.ForeignKey('post.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<Post {}>'.format(self.id)
