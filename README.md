# Project
Hacker News Clone
# Prerequisites
* python3
* sqlite
* A text editor
* terminal
* flask framework
* ORM
* Jinja2 template
* API
* HTTP
* REST
# About
this project is clone of original [hacker-news](https://news.ycombinator.com/) website